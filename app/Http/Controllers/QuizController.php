<?php

namespace App\Http\Controllers;

use App\Models\quiz;
use App\Models\statistical;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function index()
    {
        return view('start');
    }

    public function question()
    {
        return view('question');
    }

    public function loadQuestion($id)
    {
        $question = quiz::find($id);

        return response()->json([
            'question'  =>   $question
        ]);
    }

    public function finish()
    {
        return view('finish');
    }

    public function loadRes()
    {
        $res = statistical::first();

        return response()->json([
            'res'   => $res
        ]);
    }

    public function resetRes()
    {
        $res = statistical::first()->delete();
    }

    public function detail()
    {
        return view('detail');
    }
}
