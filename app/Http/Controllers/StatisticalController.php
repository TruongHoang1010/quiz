<?php

namespace App\Http\Controllers;

use App\Models\quiz;
use App\Models\statistical;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;

class StatisticalController extends Controller
{
    public function checkAnswer($ans, $id)
    {
        $check = quiz::where('id', $id)
                     ->where('correct_answer', $ans)->first();

        $res = quiz::find($id);
        $res->your_answer = $ans;
        $res->save();

        if ($check) {
            $check = true;
            $sta = statistical::first();
            if($sta) {
                $sta->correct_answer = $sta->correct_answer + 1;
                $sta->save();
            } else {
                $val['correct_answer'] = 1;
                statistical::create($val);
            }
        } else {
            $check = false;
        }

        return response()->json([
            'check' =>  $check
        ]);
    }

    public function updateTime($seconds)
    {
        $sta = statistical::first();
        $sta->time = $seconds;
        $sta->save();
    }
}
