<?php

use App\Http\Controllers\QuizController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/quiz'], function () {
    Route::get('/start', [\App\Http\Controllers\QuizController::class, 'index']);
    Route::get('/question', [\App\Http\Controllers\QuizController::class, 'question']);
    Route::get('/load-question/{id}', [\App\Http\Controllers\QuizController::class, 'loadQuestion']);

    Route::get('/check-answer/{ans}/{id}', [\App\Http\Controllers\StatisticalController::class, 'checkAnswer']);
    Route::get('/update-time/{seconds}', [\App\Http\Controllers\StatisticalController::class, 'updateTime']);

    Route::get('/finish', [\App\Http\Controllers\QuizController::class, 'finish']);
    Route::get('/load-res', [\App\Http\Controllers\QuizController::class, 'loadRes']);
    Route::get('/reset-res', [\App\Http\Controllers\QuizController::class, 'resetRes']);

    Route::get('/detail', [\App\Http\Controllers\QuizController::class, 'detail']);
});

