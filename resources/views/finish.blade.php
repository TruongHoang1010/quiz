<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Quiz</title>
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/authentication.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- END: Custom CSS-->

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js" integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div id="app" class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-basic px-2">
                    <div class="auth-inner my-2">
                        <!-- Login basic -->
                        <div class="card mb-0">
                            <div class="card-body">
                                <template v-if="res.correct_answer > 5 || res.correct_answer == 5">
                                    <div class="card card-congratulations">
                                        <div class="card-body text-center">
                                            <img src="/app-assets/images/elements/decore-left.png" class="congratulations-img-left" alt="card-img-left" />
                                            <img src="/app-assets/images/elements/decore-right.png" class="congratulations-img-right" alt="card-img-right" />
                                            <div class="avatar avatar-xl bg-primary shadow">
                                                <div class="avatar-content">
                                                    <i data-feather='award'></i>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <h1 class="mb-1 text-white">Congratulations!!</h1>
                                                <p class="card-text m-auto w-85">
                                                    <strong>@{{res.correct_answer}}/10</strong> correct answers in <strong>@{{res.time}}</strong> seconds
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                                <template v-else>
                                    <div class="card card-congratulations">
                                        <div class="card-body text-center">
                                            {{-- <img src="/app-assets/images/elements/decore-left.png" class="congratulations-img-left" alt="card-img-left" />
                                            <img src="/app-assets/images/elements/decore-right.png" class="congratulations-img-right" alt="card-img-right" /> --}}
                                            <div class="avatar avatar-xl bg-primary shadow">
                                                <div class="avatar-content">
                                                    <i data-feather='refresh-ccw'></i>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <h1 class="mb-1 text-white">Completed!</h1>
                                                <p class="card-text m-auto w-85">
                                                    <strong>@{{res.correct_answer}}/10</strong> correct answers in <strong>@{{res.time}}</strong> seconds
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                                <div class="row">
                                    <div class="col-md-6 text-start">
                                        <a href="/quiz/start" type="button" class="btn btn-primary round waves-effect text-center" v-on:click="resetRes()">Play Again</a>
                                    </div>
                                    <div class="col-md-6 text-end">
                                        <a href="/quiz/detail" type="button" class="btn btn-primary round waves-effect text-center" v-on:click="resetRes()">Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Login basic -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js"></script>
    <script src="/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/pages/auth-login.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
    <script>
        $(document).ready(function(){
            new Vue({
                el  : "#app",
                data : {
                    res : {},
                },
                created() {
                    this.loadRes();
                },
                methods : {
                    loadRes() {
                        axios
                            .get('/quiz/load-res')
                            .then((res) => {
                                this.res = res.data.res;
                            });
                    },

                    resetRes() {
                        axios
                            .get('/quiz/reset-res')
                            .then((res) => {
                            });
                    }
                },
            });
        })
    </script>
</body>
<!-- END: Body-->

</html>
