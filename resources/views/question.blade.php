<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Quiz</title>
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/authentication.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- END: Custom CSS-->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js" integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div id="app" class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-basic px-2">
                    <div class="auth-inner my-2">
                        <!-- Login basic -->
                        <div class="card mb-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6 text-center">
                                        <h1 class="text-primary">Question @{{question.id}}</h1>
                                    </div>
                                    <div class="col-md-2 text-start" style="margin-top: 10px; margin-left: -30px">
                                        <h5>/ 10</h5>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <h5 id="time" class="text-primary">Time: @{{seconds}}s</h5>
                                </div>
                                <div class="card shadow-none bg-transparent border-primary">
                                    <div class="card-body">
                                        <p class="card-text text-white text-center">@{{question.question}}</p>
                                    </div>
                                </div>
                                <button type="button" id="A" class="btn btn-outline-primary w-100 waves-effect waves-float waves-light text-start" v-on:click="answer(question.A, question.id); checkA()">A: @{{question.A}}</button>
                                <button type="button" id="B" class="btn btn-outline-primary w-100 waves-effect waves-float waves-light mt-1 text-start" v-on:click="answer(question.B, question.id); checkB()">B: @{{question.B}}</button>
                                <button type="button" id="C" class="btn btn-outline-primary w-100 waves-effect waves-float waves-light mt-1 text-start" v-on:click="answer(question.C, question.id); checkC()">C: @{{question.C}}</button>
                                <button type="button" id="D" class="btn btn-outline-primary w-100 waves-effect waves-float waves-light mt-1 text-start" v-on:click="answer(question.D, question.id); checkD()">D: @{{question.D}}</button>
                                <div class="text-center mt-1">
                                    <template v-if="question.id < 10">
                                        <button type="button" class="next btn btn-primary w-50 round waves-effect text-center" v-on:click="loadQuestion(question.next); format()" disabled>Next</button>
                                    </template>
                                    <template v-else>
                                        <a href="/quiz/finish" type="button" class="next btn btn-primary w-50 round waves-effect text-center" v-on:click="updateTime(seconds)" disabled>Finish</a>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Content-->
    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js"></script>
    <script src="/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/pages/auth-login.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
    <script>
        $(document).ready(function(){
            new Vue({
                el  : "#app",
                data : {
                    question    : {},
                    check       : false,
                    ans         : '',
                    seconds     : 0,
                },
                created() {
                    this.loadQuestion(1);
                    setInterval(() => {
                    this.seconds++;
                }, 1000);
                },
                methods : {
                    loadQuestion(id){
                        axios
                            .get('/quiz/load-question/' + id)
                            .then((res) => {
                                this.question   = res.data.question;
                            });
                    },

                    updateTime(seconds){
                        axios
                            .get('/quiz/update-time/' + seconds)
                            .then((res) => {
                            });
                    },

                    answer(ans, id){
                        $('.next').removeAttr("disabled")
                        axios
                            .get('/quiz/check-answer/' + ans + '/' + id)
                            .then((res) => {
                                this.check = res.data.check;
                                if(this.ans == 'A') {
                                    this.A();
                                } else if( this.ans == 'B') {
                                    this.B();
                                } else if( this.ans == 'C') {
                                    this.C();
                                } else {
                                    this.D();
                                }
                                $('#A').attr("disabled", "disabled");
                                $('#B').attr("disabled", "disabled");
                                $('#C').attr("disabled", "disabled");
                                $('#D').attr("disabled", "disabled");
                            });
                    },

                    checkA(){
                        this.ans = 'A';
                    },

                    checkB(){
                        this.ans = 'B';
                    },

                    checkC(){
                        this.ans = 'C';
                    },

                    checkD(){
                        this.ans = 'D';
                    },

                    A(){

                        if(this.check == true) {
                            document.getElementById("A").classList.add('btn-outline-success');
                            document.getElementById("A").classList.remove('btn-outline-primary');
                        } else {
                            document.getElementById("A").classList.add('btn-outline-danger');
                            document.getElementById("A").classList.remove('btn-outline-primary');
                        }
                    },

                    B(){
                        if(this.check == true) {
                            document.getElementById("B").classList.add('btn-outline-success');
                            document.getElementById("B").classList.remove('btn-outline-primary');
                        } else {
                            document.getElementById("B").classList.add('btn-outline-danger');
                            document.getElementById("B").classList.remove('btn-outline-primary');
                        }
                    },

                    C(){
                        if(this.check == true) {
                            document.getElementById("C").classList.add('btn-outline-success');
                            document.getElementById("C").classList.remove('btn-outline-primary');
                        } else {
                            document.getElementById("C").classList.add('btn-outline-danger');
                            document.getElementById("C").classList.remove('btn-outline-primary');
                        }
                    },

                    D(){
                        if(this.check == true) {
                            document.getElementById("D").classList.add('btn-outline-success');
                            document.getElementById("D").classList.remove('btn-outline-primary');
                        } else {
                            document.getElementById("D").classList.add('btn-outline-danger');
                            document.getElementById("D").classList.remove('btn-outline-primary');
                        }
                    },

                    format(){
                        document.getElementById("A").classList.remove('btn-outline-success');
                        document.getElementById("A").classList.remove('btn-outline-danger');
                        document.getElementById("A").classList.add('btn-outline-primary');

                        document.getElementById("B").classList.remove('btn-outline-success');
                        document.getElementById("B").classList.remove('btn-outline-danger');
                        document.getElementById("B").classList.add('btn-outline-primary');

                        document.getElementById("C").classList.remove('btn-outline-success');
                        document.getElementById("C").classList.remove('btn-outline-danger');
                        document.getElementById("C").classList.add('btn-outline-primary');

                        document.getElementById("D").classList.remove('btn-outline-success');
                        document.getElementById("D").classList.remove('btn-outline-danger');
                        document.getElementById("D").classList.add('btn-outline-primary');

                        $('#A').removeAttr("disabled")
                        $('#B').removeAttr("disabled")
                        $('#C').removeAttr("disabled")
                        $('#D').removeAttr("disabled")

                    }
                },
            });
        })
    </script>
</body>
<!-- END: Body-->

</html>
