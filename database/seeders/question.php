<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class question extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('quizzes')->delete();

        DB::table('quizzes')->truncate();

        DB::table('quizzes')->insert([
            [
                'question' => 'Một chú ốc sên bò từ gốc đến ngọn của một cái cây 5m. Ban ngày chú bò được 2m, ban đêm chú ngủ nên bị tuột xuống 1m. Hỏi đến ngày thứ mấy thì chú ốc sên bò lên đến được ngọn cây?',
                'A' => '4',
                'B' => '5',
                'C' => '6',
                'D' => '7',
                'correct_answer' => '4',
                'next' => 2,
                'previous'  => 0
            ],
            [
                'question' => 'Nước Mỹ có bao nhiêu bang?',
                'A' => '48',
                'B' => '49',
                'C' => '50',
                'D' => '51',
                'correct_answer' => '50',
                'next' => 3,
                'previous'  => 1
            ],
            [
                'question' => '18 / 3 x (1 + 2)',
                'A' => '2',
                'B' => '18',
                'C' => '12',
                'D' => '20',
                'correct_answer' => '18',
                'next' => 4,
                'previous'  => 2
            ],
            [
                'question' => 'H2 + O2 -> ?',
                'A' => 'HOHO',
                'B' => 'HHOO',
                'C' => 'OHOH',
                'D' => 'H2O',
                'correct_answer' => 'H2O',
                'next' => 5,
                'previous'  => 3
            ],
            [
                'question' => 'Châu lục có diện tích lớn thứ 3 trên thế giới?',
                'A' => 'Châu Đại Dương',
                'B' => 'Châu Nam Cực',
                'C' => 'Châu Mỹ',
                'D' => 'Châu Phi',
                'correct_answer' => 'Châu Phi',
                'next' => 6,
                'previous'  => 4
            ],
            [
                'question' => 'Trung bình cộng số tuổi của cô giáo chủ nhiệm và 32 học sinh trong lớp 4A là 13 tuổi. Trung bình cộng số tuổi của 32 học sinh lớp 4A là 12 tuổi. Hỏi cô giáo năm nay bao nhiêu tuổi?',
                'A' => '44',
                'B' => '45',
                'C' => '46',
                'D' => '47',
                'correct_answer' => '45',
                'next' => 7,
                'previous'  => 5
            ],
            [
                'question' => 'Quang Trung và Nguyễn Hệ là gì của nhau?',
                'A' => 'Là 2 anh em ruột',
                'B' => 'Là kẻ thù của nhau',
                'C' => 'Là tên của một vị vua',
                'D' => 'Là 2 vị anh hùng đã chiến đấu cùng nhau',
                'correct_answer' => 'Là tên của một vị vua',
                'next' => 8,
                'previous'  => 6
            ],
            [
                'question' => 'Khi một người ngồi trong một chiếc ô tô đang chạy với tốc độ cao, đột nhiên chiếc xe thằng gấp làm người lái xe ngã về phía trước. Vậy hiện tượng người lái xe ngã về phía trước là gọi là lực gì?',
                'A' => 'Lực quán tính',
                'B' => 'Lực xoay ly tâm',
                'C' => 'Lực ma sát',
                'D' => 'Lực hướng tâm',
                'correct_answer' => 'Lực quán tính',
                'next' => 9,
                'previous'  => 7
            ],
            [
                'question' => 'Tác giả của "Truyện Kiều" là ai?',
                'A' => 'Nguyễn Du',
                'B' => 'Nguyễn Khoa Điềm',
                'C' => 'Trần Đăng Khoa',
                'D' => 'Xuân Diệu',
                'correct_answer' => 'Nguyễn Du',
                'next' => 10,
                'previous'  => 8
            ],
            [
                'question' => 'Ai là người lấy thân mình lấp lỗ châu mai?',
                'A' => 'Tô Vĩnh Diện',
                'B' => 'Phan Đình Giót',
                'C' => 'Bế Văn Đàn',
                'D' => 'Trần Can',
                'correct_answer' => 'Phan Đình Giót',
                'next' => 0,
                'previous'  => 9
            ],
        ]);

    }
}
